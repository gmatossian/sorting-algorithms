package linearithmic;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class HeapSortParameterizedTest {

	@Parameterized.Parameters
    public static Collection<Integer[][]> testCases() {
        return Arrays.asList(new Integer[][][] {
        	{{}, {}},
        	{{ 0 }, { 0 }},
        	{{ 0, 0 }, { 0, 0 }},
        	{{ 0, 0, 0 }, { 0, 0, 0 }},
        	{{ 0, 1 }, { 0, 1 }},
        	{{ 1, 0 }, { 0, 1 }},
        	{{ 0, 1, 2 }, { 0, 1, 2 }},
        	{{ 0, 2, 1 }, { 0, 1, 2 }},
        	{{ 1, 0, 2 }, { 0, 1, 2 }},
        	{{ 1, 2, 0 }, { 0, 1, 2 }},
        	{{ 2, 0, 1 }, { 0, 1, 2 }},
        	{{ 2, 1, 0 }, { 0, 1, 2 }},
        	{{ 0, 1, 1 }, { 0, 1, 1 }},
        	{{ 1, 0, 1 }, { 0, 1, 1 }},
        	{{ 1, 1, 0 }, { 0, 1, 1 }},
        	{{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 }, { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 }},
        	{{ 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 }, { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 }},
        	{{ 42, 9, 17, 54, 602, -3, 54, 999, -11 }, { -11, -3, 9, 17, 42, 54, 54, 602, 999 }},
        	{{ -11, -3, 9, 17, 42, 54, 54, 602, 999 }, { -11, -3, 9, 17, 42, 54, 54, 602, 999 }},
        	{ { 5, 5, 6, 6, 4, 4, 5, 5, 4, 4, 6, 6, 5, 5 }, { 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 6, 6, 6, 6 } }
    	});
    }
	
	private Integer[] input;
	private Integer[] expected;
	
	public HeapSortParameterizedTest(Integer[] input, Integer[] expected) {
		this.input = input;
		this.expected = expected;
	}

	@Test
	public void testSort() {
		HeapSort heapSort = new HeapSort(input);
		heapSort.sort();
		
		Assert.assertArrayEquals(expected, input);
	}
}
