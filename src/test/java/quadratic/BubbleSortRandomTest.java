package quadratic;

import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class BubbleSortRandomTest {

	private Integer[] items;
	static final int MAX_ARRAY_SIZE = 10;
	static final int MAX_ELEMENT = 20;
	static final int NUMBER_OF_RUNS = 100;
	
	@Parameterized.Parameters
    public static List<Object[]> data() {
        return Arrays.asList(new Object[NUMBER_OF_RUNS][0]);
    }
    

	@Before
	public void setUp() throws Exception {
		Random random = new Random();

		items = new Integer[random.nextInt(MAX_ARRAY_SIZE)];
		for (int i = 0; i < items.length; i++) {
			items[i] = random.nextInt(MAX_ELEMENT);
		}
	}

	@Test
	public void testSort() {
		Integer[] input = Arrays.copyOf(items, items.length);
		
		BubbleSort.sort(items);

		if (!isSorted(items)) {
			fail("Array is not sorted: input = " + Arrays.toString(input) + ", sorted items = " + Arrays.toString(items));
		}
	}

	private boolean isSorted(Integer[] numbers) {
		for (int i = 0; i < numbers.length - 1; i++) {
			if (numbers[i] > numbers[i + 1]) {
				return false;
			}
		}
		return true;
	}
}
