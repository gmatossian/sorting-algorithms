package external;

import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Random;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ExternalSortTest {
	
	private File input;
	private File output;
	
	@Before
	public void createdInputFile() throws Exception {
		input = File.createTempFile("input", "temp");
		output = File.createTempFile("output", "temp");
		createInputFile();
	}
	
	@After
	public void deleteAllFiles() {
		if(input.exists()) input.delete();
		if(output.exists()) output.delete();
	}
	
	@Test
	public void testSort() throws Exception {
		ExternalSort externalSort = new ExternalSort(input, output);
		externalSort.sort();
		
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(output), "UTF8"))) {
			int c;
			int prev = 'a';
			while((c = reader.read()) != -1) {
				if(c < prev) {
					fail();
				}
			}
		}
	}

	private void createInputFile() throws Exception {
		char [] buffer = new char[100 * 1024];	// 100 KB
		
		Random random = new Random();
		
		try(BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(input), "UTF-8"))) {
			for(int i = 0; i < 9; i++) {	// Write 100KB of random chars, 9 times
				for(int j = 0; j < buffer.length; j++) {
					buffer[j] = (char) (random.nextInt(26) + 'a');
				}
				writer.write(buffer);
			}
		}
		
	}
}