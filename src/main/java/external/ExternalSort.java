package external;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;

public class ExternalSort {
	
	private File input;
	private File output;

	public ExternalSort(File input, File output) {
		this.input = input;
		this.output = output;
	}
	
	public void sort() throws Exception {
		char [] buffer = new char[100 * 1024];	// 100KB

		int chunks = 0;
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(input), "UTF8"))) {
			while(reader.read(buffer) > 0) {
				Arrays.sort(buffer);	// Uses dual-pivot quicksort
				
				File chunk = new File("chunk" + chunks  + ".txt");
				try(BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(chunk), "UTF-8"))) {
					writer.write(buffer);
				}
				chunks++;
			}
		}
		
		merge(chunks);
		
		for (int i = 0; i < chunks; i++) {	// Remove chunk files
			File chunk = new File("chunk" + i  + ".txt");
			chunk.delete();
		}
	}

	private void merge(int chunks) throws Exception {
		char [] outputBuffer = new char[10 * 1024];	// 10 KB
		int next = 0;
		
		PriorityQueue<Chunk> queue = new PriorityQueue<>(chunks, new Comparator<Chunk>() {
			@Override
			public int compare(Chunk o1, Chunk o2) {
				return Character.valueOf(o1.buffer[o1.next])
						.compareTo(Character.valueOf(o2.buffer[o2.next]));
			}
		});
		
		for(int i = 0; i < chunks; i++) {
			Chunk chunk = new Chunk();
			chunk.file = new File("chunk" + i  + ".txt");
			chunk.buffer = new char[10 * 1024];	// 10 KB
			chunk.next = 0;
			chunk.available = 0;
			chunk.read = 0;
			
			try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(chunk.file), "UTF8"))) {
				chunk.available = reader.read(chunk.buffer);
				chunk.read = chunk.available;
				queue.add(chunk);
			}
		}
		
		while(!queue.isEmpty()) {
			Chunk chunk = queue.poll();
			
			char c = chunk.buffer[chunk.next++];
			outputBuffer[next++] = c;
			
			if(next == outputBuffer.length) {	// If output buffer is full then write to output file and reset it
				try(BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(output, true), "UTF-8"))) {
					writer.write(outputBuffer);
				}
				next = 0;
			}
			
			if(chunk.next == chunk.available) {
				try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(chunk.file), "UTF8"))) {
					reader.skip(chunk.read);
					chunk.available = reader.read(chunk.buffer);
					chunk.read = chunk.read + chunk.available;
				}
				if(chunk.available > 0) {
					chunk.next = 0;
					queue.add(chunk);
				}
			} else if(chunk.next < chunk.available) {
				queue.add(chunk);
			}
		}
		
		// Flushing output buffer...
		try(BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(output, true), "UTF-8"))) {
			writer.write(outputBuffer, 0, next);
		}
	}
}

class Chunk {
	File file;
	int read;
	char[] buffer;
	int available;
	int next;
}
