package quadratic;

public class BubbleSort {
	
	public static void sort(Integer[] items) {
		Integer limit;
		Integer lastSwap = items.length - 1;
		
		do {
			/*
			 * The limit is the position of the right-most unsorted item,
			 * initially is n-1 since the entire list is unsorted
			 */
			limit = lastSwap;
			
			/*
			 * If the last swap location is 0 (either because there were no swaps or because
			 * the last swap was actually in the index 0) then the array is considered fully sorted
			 */
			lastSwap = 0;
			for(int i = 0; i < limit; i++) {
				if(items[i+1] < items[i]) {
					swap(items, i, i+1);
					lastSwap = i;
				}
			}
		} while(lastSwap != 0);
	}
	
	private static void swap(Integer[] items, int i, int j) {
		Integer aux = items[i];
		items[i] = items[j];
		items[j] = aux;
	}
}
