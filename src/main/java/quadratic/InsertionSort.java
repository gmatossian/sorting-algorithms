package quadratic;

public class InsertionSort {
	
	public static void sort(Integer[] items) {
		/*
		 * One iteration per index is used, but starting from index 1
		 * since items are compared against the one on their left
		 */
		for(int i = 1; i < items.length; i++) {
			if(items[i] < items[i-1]) {
				Integer aux = items[i];	// The value will be overwritten when shifting items to make space
				Integer j = i-1;
				/*
				 * Shifts items as long as they're bigger, it stops when the item is not bigger
				 * or it goes out of boundaries
				 */
				while(j >= 0 && aux < items[j]) {
					items[j+1] = items[j];
					j--;
				}
				/*
				 * When the loop exits, it means that one of the following happened:
				 * - A: It went out of boundaries (j = -1)
				 * - B: aux is not smaller than the element at j
				 * Either way, it means the item needs to be inserted in j+1
				 */
				items[j+1] = aux;
			}
		}
	}
}
