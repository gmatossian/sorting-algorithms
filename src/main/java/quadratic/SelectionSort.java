package quadratic;

public class SelectionSort {
	
	public static void sort(Integer[] items) {
		/*
		 * The rightmost item doesn't need to be processed,
		 * the array will be fully sorted after the first n-1 are processed
		 */
		for(int i = 0; i < items.length-1; i++) {
			int smallestItem = i;
			for(int j = i + 1; j < items.length; j ++) {
				if(items[j] < items[smallestItem]) {
					smallestItem = j;
				}
			}
			swap(items, i, smallestItem);
		}
	}
	
	private static void swap(Integer[] items, int i, int j) {
		Integer aux = items[i];
		items[i] = items[j];
		items[j] = aux;
	}
}
