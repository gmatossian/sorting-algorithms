package linearithmic;

public class MergeSort {
	
	private Integer[] items;
	private Integer[] aux;

	public MergeSort(Integer[] items) {
		this.items = items;
		this.aux = new Integer[items.length];
	}
	
	public void sort() {
		/*
		 * Initially, low and high are set to the positions of
		 * the first and last items respectively
		 */
		mergeSort(0, items.length-1);
	}
	
	private void mergeSort(int low, int high) {
		if(low < high) {
			int middle = low + (high - low) / 2;
			mergeSort(low, middle);
			mergeSort(middle + 1, high);
			merge(low, middle, high);
		}
	}
	
	private void merge(int low, int middle, int high) {
		for(int i = low; i <= high; i++) {
			aux[i] = items[i];
		}
		
		int i = low;
		int j = middle + 1;
		int k = low;
		while(i <= middle && j <= high) {
			if(aux[i] <= aux[j]) {
				items[k] = aux[i];
				i++;
				k++;
			} else {
				items[k] = aux[j];
				j++;
				k++;
			}
		}
		while(i <= middle) {
			items[k] = aux[i];
			i++;
			k++;
		}
	}
}
