package linearithmic;

public class QuickSortHoareScheme {
	
	private Integer[] items;

	public QuickSortHoareScheme(Integer[] items) {
		this.items = items;
	}
	
	public void sort() {
		if(items != null && items.length > 1) {
			quickSort(0, items.length - 1);
		}
	}

	private void quickSort(int low, int high) {
		int pivot = items[low + (high - low) / 2];
		int i = low - 1;
		int j = high + 1;
		
		while(i < j) {
			do {
				i++;
			} while(items[i] < pivot);

			do {
				j--;
			} while(items[j] > pivot);
			
			if(i < j) {
				swap(i, j);
			}
		}
		
		if(low < j)
			quickSort(low, j);
		if(j+1 < high)
			quickSort(j + 1, high);
	}
	
	private void swap(int i, int j) {
		Integer aux = items[i];
		items[i] = items[j];
		items[j] = aux;
	}
}
