package linearithmic;

public class HeapSort {

	private Integer[] items;
	
	public HeapSort(Integer[] items) {
		this.items = items;
	}
	
	public void sort() {
		/*
		 * First step: build max heap
		 * That is, restore the heap property
		 */
		buildMaxHeap();
		
		/*
		 * The size of the heap is initially the entire array
		 * It gets decreased every time we extract the root
		 */
		int heapSize = items.length;
		
		/*
		 * Iterates over the max heap, taking the root element
		 * on each iteration (which is the max element in the heap),
		 * putting it at the end of the range containing the heap,
		 * putting the element that was at the end of the heap in the root
		 * and applying siftDown to the root element to re-establish the heap property
		 */
		for (int i = items.length -1; i > 0; i--) {
			int aux = items[i];
			items[i] = items[0];
			items[0] = aux;
			heapSize--;
			siftDown(0, heapSize);
		}
	}
	
	private void buildMaxHeap() {
		/*
		 * It iterates over the parent elements (parent of i being (i-1)/2)
		 * applying siftDown to them to establish the heap property
		 */
		for (int i = (items.length-1)/2; i >= 0; i--) {
			siftDown(i, items.length);
		}
	}

	private void siftDown(int index, int numberOfItems) {
		int leftIdx = 2 * index + 1;
		int rightIdx = 2 * index + 2;
		int largestItemIdx = index;
		
		if(leftIdx < numberOfItems && items[leftIdx] > items[largestItemIdx])
			largestItemIdx = leftIdx;
		if(rightIdx < numberOfItems && items[rightIdx] > items[largestItemIdx])
			largestItemIdx = rightIdx;
		
		if(largestItemIdx != index) {
			swap(largestItemIdx, index);
			siftDown(largestItemIdx, numberOfItems);
		}
	}

	private void swap(int newNodeIndex, int parentIndex) {
		int aux = items[newNodeIndex];
		items[newNodeIndex] = items[parentIndex];
		items[parentIndex] = aux;
	}
}
