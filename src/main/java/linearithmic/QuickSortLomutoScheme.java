package linearithmic;

public class QuickSortLomutoScheme {
	
	private Integer[] items;

	public QuickSortLomutoScheme(Integer[] items) {
		this.items = items;
	}
	
	public void sort() {
		if(items != null && items.length > 1) {
			quickSort(0, items.length - 1);
		}
	}

	private void quickSort(int low, int high) {
		int pivot = items[high];
		int i = low;
		
		for(int j = low; j < high; j++) {
			if(items[j] < pivot) {
				swap(i, j);
				i++;
			}
		}
		
		swap(i, high);
		items[i] = pivot;
		
		if(i-1 > low) {
			quickSort(low, i-1);
		}
		if(i+1 < high) {
			quickSort(i+1, high);
		}
	}
	
	private void swap(int i, int j) {
		Integer aux = items[i];
		items[i] = items[j];
		items[j] = aux;
	}
}
