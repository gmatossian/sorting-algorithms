package parallel;

public class ParallelMergeSort {
	
	private Integer[] items;
	private Integer[] aux;

	public ParallelMergeSort(Integer[] items) {
		this.items = items;
		this.aux = new Integer[items.length];
	}
	
	public void sort() {
		parallelMergeSort(0, items.length-1, Runtime.getRuntime().availableProcessors());
	}
	
	private void parallelMergeSort(int low, int high, int numOfThreads) {
		if(numOfThreads <= 1) {
			mergeSort(low, high);
		} else {
			if (low < high) {
				int middleIndex = low + (high - low) / 2;
				
				Thread leftSorter = new Thread() {
					@Override
					public void run() {
						parallelMergeSort(low, middleIndex, numOfThreads / 2);
					}
				};
				
				Thread rightSorter = new Thread() {
					@Override
					public void run() {
						parallelMergeSort(middleIndex+1, high, numOfThreads / 2);
					}
				};
				
				leftSorter.start();
				rightSorter.start();
				
				try {
					leftSorter.join();
					rightSorter.join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				merge(low, middleIndex, high);
			}
		}
	}
	
	private void mergeSort(int low, int high) {
		if(low < high) {
			int middle = low + (high - low) / 2;
			mergeSort(low, middle);
			mergeSort(middle + 1, high);
			merge(low, middle, high);
		}
	}
	
	private void merge(int low, int middle, int high) {
		for(int i = low; i <= high; i++) {
			aux[i] = items[i];
		}
		
		int i = low;
		int j = middle + 1;
		int k = low;
		while(i <= middle && j <= high) {
			if(aux[i] <= aux[j]) {
				items[k] = aux[i];
				i++;
				k++;
			} else {
				items[k] = aux[j];
				j++;
				k++;
			}
		}
		while(i <= middle) {
			items[k] = aux[i];
			i++;
			k++;
		}
	}
}
